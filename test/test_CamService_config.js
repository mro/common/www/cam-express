// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)

import { expect } from "chai";
import { afterEach, beforeEach, describe, it } from "mocha";
import axios from "axios";
import { isNil } from "@cern/nodash";
import { HTTPServer } from "./utils/index.js";
import CamService from "../src/CamService.js";
import config from "./config-stub.js";

const camApp = config.camApp;

describe("Cam Service config", function() {
  let env = {};

  beforeEach(async function() {
    env.httpServer = new HTTPServer({});
    await env.httpServer.listen();
  });

  afterEach(function() {
    env.httpServer?.close?.();
    env.camService?.release?.();
    env = {};
  });


  it("provides cameras configuration", function() {
    env.camService = new CamService(camApp);
    env.camService.register(env.httpServer.app);
    const appSrvPort = env.httpServer.address().port;
    return axios.get(`http://localhost:${appSrvPort}/config`)
    .then((res) => {
      expect(res.status).to.be.equal(200);

      const camConfig = Object.entries(camApp.groups).reduce(
        (ret, [ key, group ]) => {
          ret[key] = {
            description: group?.description ?? "",
            cameras: Object.values(group?.cameras ?? {}).map((camera) => (
              Object.fromEntries(Object.entries({
                name: camera.name,
                description: camera.description ?? "",
                ptz: camera.ptz ? true : null,
                screenshot: camera.screenshot ? true : null
              }).filter(([ _key, value ]) => !isNil(value)))))
          };
          return ret;
        }, {});

      expect(res.data).to.be.deep.equal(camConfig);
    });
  });

  it("raises an error when configuration is missing", function() {
    env.camService = new CamService(camApp);
    env.camService.register(env.httpServer.app);
    const appSrvPort = env.httpServer.address().port;

    // overwrite camera configuration
    env.camService.camConfig = {};

    return axios.get(`http://localhost:${appSrvPort}/config`)
    .then(() => expect.fail("Should fail"))
    .catch((err) => {
      expect(err?.response?.status).to.be.equal(500);
      expect(err?.response?.data).to.be.equal("Camera configuration missing");
    });
  });

  it("supports extra config info", async function() {
    env.camService = new CamService({
      groups: {
        group1: {
          cameras: [ { name: "cam1", url: "", extra: { tea: "pot" } } ]
        }
      },
      credentials: {}
    });
    env.camService.register(env.httpServer.app);

    const url = `http://localhost:${env.httpServer.address().port}/config`;
    const ret = await axios.get(url);
    expect(ret.status).to.equal(200);
    expect(ret.data).to.have.nested.property("group1.cameras.0.tea", "pot");
  });
});
