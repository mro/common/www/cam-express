// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)
// SPDX-Created: 2024-02-05T12:03:20
// SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

import MultiPart from "./MultiPart.js";
import { describe, it } from "mocha";
import { Readable } from "node:stream";
import { expect } from "chai";
import { AxiosHeaders } from "axios";

class AxiosResponseStub {
  status = 200;

  statusText = "OK";

  config = { headers: new AxiosHeaders("") };

  headers = {
    "Content-Type": "multipart/x-mixed-replace; boundary=frame"
  };

  /**
   *
   * @param {Readable|any} data
   */
  constructor(data) {
    this.data = data;
  }
}


describe("MultiPart", function() {
  it("can parse a document", async function() {
    const data = [
      "Cache-Control: no-store",
      "Content-Type: multipart/x-mixed-replace; boundary=frame\r\n",

      "--frame",
      "Content-Type: text/plain",
      "Content-Length: 4\r\n",
      "test",

      "trash data",

      "--frame",
      "Content-Type: text/plain",
      "content-length: 5\r\n",
      "test2"
    ].map((v) => Buffer.from(v + "\r\n"));
    const readable = Readable.from(data);
    const multipart = new MultiPart(new AxiosResponseStub(readable));

    const ret = [];
    multipart.on("part", (contentType, data) => ret.push(
      { contentType, data: data.toString() }));

    await multipart.promise;

    expect(ret).to.deep.equal([
      { contentType: "text/plain", data: "test" },
      { contentType: "text/plain", data: "test2" }
    ]);
  });

  it("rejects non stream requests", async function() {
    expect(() => new MultiPart(new AxiosResponseStub(null)))
    .to.throw(Error, "stream");
  });
});


