// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)

import { expect } from "chai";
import axios from "axios";
import d from "debug";
import { afterEach, beforeEach, describe, it } from "mocha";

import { HTTPServer } from "./utils/index.js";
import { CamService } from "../src/index.js";

const debug = d("tests");

class PTZStub {
  constructor() {
    this.ptz = {
      pan: 0.0000,
      tilt: 0.0000,
      zoom: 200,
      iris: 6248,
      focus: 9854,
      brightness: 4999,
      autofocus: "on",
      autoiris: "on"
    };
    this.limits = {
      MinPan: -170,
      MaxPan: 170,
      MinTilt: -20,
      MaxTilt: 90,
      MinZoom: 1,
      MaxZoom: 9999,
      MinIris: 1,
      MaxIris: 9999,
      MinFocus: 770,
      MaxFocus: 9999,
      MinFieldAngle: 20,
      MaxFieldAngle: 570,
      MinBrightness: 1,
      MaxBrightness: 9999
    };
  }

  register(router) {
    router.get("/axis-cgi/com/ptz.cgi", (req, res) => {
      debug("ptz request", req.query);
      if (req.query["query"] === "limits") {
        res.send(Object.entries(this.limits ?? {}).map(
          ([ key, value ]) => `${key}=${value}`).join("\r\n"));
      }
      else if (req.query["query"] === "position") {
        res.send(Object.entries(this.ptz ?? {}).map(
          ([ key, value ]) => `${key}=${value}`).join("\r\n"));
      }
      else {
        Object.entries(this.ptz ?? {}).forEach(([ name, v ]) => {
          if (req.query[name]) {
            this.ptz[name] = Number.isFinite(v) ?
              Number(req.query[name]) : req.query[name];
          }
          if (req.query["r" + name]) {
            this.ptz[name] += Number(req.query["r" + name]);
          }
        });
        res.status(200).send(true);
      }
    });
  }
}


describe("AxisPTZ", function() {
  let env = {};

  beforeEach(async function() {
    env.httpServer = new HTTPServer({});
    await env.httpServer.listen();

    env.ptz = new PTZStub();
    env.ptz.register(env.httpServer.app);
  });

  afterEach(function() {
    env.httpServer?.close?.();
    env.camService?.release?.();
    env = {};
  });

  it("can query an AxisPTZ", async function() {
    env.camService = new CamService({
      groups: {
        group1: {
          cameras: [
            {
              name: "cam1", url: "",
              ptz: {
                type: "Axis",
                url: `http://localhost:${env.httpServer.address().port}` }
            },
            { name: "cam2", url: "" }
          ]
        }
      },
      credentials: {}
    });
    env.camService.register(env.httpServer.app);

    const url = `http://localhost:${env.httpServer.address().port}` +
      "/cameras/group1/cam1/ptz";
    let ret = await axios.get(url);
    expect(ret.status).to.equal(200);
    expect(ret.data).to.deep.equal({
      pan: 0, tilt: 0, zoom: 200, iris: 6248, focus: 9854, brightness: 4999,
      autofocus: true, autoiris: true
    });

    ret = await axios.get(url + "/limits");
    expect(ret.status).to.equal(200);
    expect(ret.data).to.deep.equal(env.ptz.limits);

    ret = await axios.put(url, { autofocus: false, tilt: 42 });
    expect(ret.status).to.equal(200);
    expect(env.ptz.ptz.autofocus).to.equal("off");
    expect(env.ptz.ptz.tilt).to.equal(42);

    ret = await axios.post(url, { autofocus: true, tilt: -2 });
    expect(ret.status).to.equal(200);
    expect(env.ptz.ptz.autofocus).to.equal("on");
    expect(env.ptz.ptz.tilt).to.equal(40);

    ret = await axios.get(`http://localhost:${env.httpServer.address().port}` +
      "/cameras/group1/cam2/ptz")
    .then(
      () => { throw new Error("should have failed"); },
      (err) => err);
    expect(ret?.response?.status).to.equal(404);
  });
});
