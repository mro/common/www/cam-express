// @ts-check

import { expect } from "chai";
import url from "url";
import { afterEach, beforeEach, describe, it } from "mocha";
import axios from "axios";
import { cloneDeep, set } from "@cern/nodash";
import WebSocket from "ws";
import { HTTPServer } from "./utils/index.js";
import config from "./config-stub.js";
import AppServer from "./utils/CamServer.js";

const { camApp, stubs } = config;

describe("Swagger exposition", function() {
  let env = {};

  beforeEach(async function() {
    env.server = new HTTPServer(stubs);

    await env.server.listen();

    // update camera config with target server port
    const port = env.server.address().port;
    const groups = Object.entries(camApp.groups)
    .reduce((ret, [ gName, group ]) => {
      set(ret, [ gName, "cameras" ], []);
      Object.values(group.cameras ?? []).forEach((cam) => {
        ret[gName].description = group?.description;
        ret[gName].cameras.push(Object.assign(cloneDeep(cam), {
          name: cam.name,
          url: cam?.url ?
            `http://localhost:${port}` + url.parse(cam.url, true).path :
            undefined,
          poll: false,
          throttle: 1000
        }));
      });
      return ret;
    }, {});

    // app server
    env.appServer = new AppServer({
      basePath: "",
      camApp: {
        groups,
        credentials: camApp.credentials
      }
    });

    await env.appServer.listen();
  });


  afterEach(function() {
    if (env.WScli?.readyState === WebSocket.OPEN) {
      env.WScli.close();
    }
    env.appServer?.close();
    env.server?.close();
    env = {};
  });

  it("provides json open-api doc", function() {
    const appSrvPort = env.appServer.address().port;
    return axios.get(`http://localhost:${appSrvPort}/api-docs.json`)
    .then((res) => {
      expect(res.status).to.be.equal(200);

      // static infos
      const keywords = [
        "cam-express", "/config", "/cameras/{group}/{camera}",
        "/cameras/{group}/{camera}/ptz",
        "/cameras/{group}/{camera}/screenshot",
        "/cameras/group1/cam1/ptz",
        "/cameras/group1/cam1/screenshot"
      ];

      keywords.forEach((keyword) => {
        expect(JSON.stringify(res.data)).to.include(keyword);
      });

      // dynamic infos
      const cameras = [];
      Object.entries(camApp.groups ?? {}).forEach(([ name, group ]) => {
        Object.values(group.cameras ?? []).forEach((cam) => {
          cameras.push(`${name}/${cam.name}`);
        });
      });

      cameras?.forEach((camera) => {
        expect(JSON.stringify(res.data)).to.include(camera);
      });
    });
  });

  it("provides swagger doc", function() {
    const appSrvPort = env.appServer.address().port;
    return axios.get(`http://localhost:${appSrvPort}/api-docs`)
    .then((res) => {
      expect(res.status).to.be.equal(200);

      expect(res.data?.toString()).to.include("Swagger UI");
    });
  });

});
