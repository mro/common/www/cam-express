// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)

import { expect } from "chai";
import { afterEach, beforeEach, describe, it } from "mocha";

import { HTTPServer, waitFor, waitForEvent, waitForValue } from "./utils/index.js";
import config from "./config-stub.js";
import { HTTPConsumer } from "../src/index.js";

describe("HTTP Consumer", function() {
  let env = {};

  beforeEach(async function() {
    env.httpServer = new HTTPServer(config.stubs);
    await env.httpServer.listen();

    const port = env.httpServer.address().port;

    const cameras = Object.keys(config.stubs ?? {});
    const cam = cameras[0];

    env.ressource = env.httpServer.ressources[cam];
    env.consumerOpts = {
      src: `http://localhost:${port}` + config.stubs[cam]?.path || "",
      auth: config.stubs[cam]?.auth,
      throttle: 0 // no dealy
    };
    env.consumer = undefined;
  });

  afterEach(function() {
    env.httpServer?.close(); // will disconnect the consumer
    env?.consumer?.disconnect();
    env = {};
  });

  it("can receive one image",
    async function() {
      let received = "";

      env.consumer = new HTTPConsumer(env.consumerOpts);
      env.consumer.on("frame", (frame) => { received = frame?.toString(); });
      env.consumer.connect();

      await waitFor(() => received.includes("IMAGE_BUFFER"));
    });

  it("can receive multiples images",
    async function() {
      const TIMEOUT = 500;
      const NB_IMAGES = 10;

      this.timeout(TIMEOUT * NB_IMAGES); // increase timeout for multiple requests

      let received = "";

      const checkFrameValid = () => received.includes("IMAGE_BUFFER");

      env.consumer = new HTTPConsumer(env.consumerOpts);
      env.consumer.on("frame", (frame) => { received = frame?.toString(); });
      env.consumer.connect();

      for (let i = 0; i < NB_IMAGES; i++) {
        await waitForValue(checkFrameValid, true, undefined, TIMEOUT);
        received = ""; // reset for next frame
      }
    });

  it("stops polling upon disconnection",
    async function() {
      let received = 0;
      const TIMEOUT = 500;
      const NB_IMAGES = 4;

      this.timeout(TIMEOUT * NB_IMAGES); // increase timeout for multiple requests

      env.consumer = new HTTPConsumer(env.consumerOpts);
      env.consumer.on("frame", () => {
        if (++received >= (NB_IMAGES >> 1)) {
          env.consumer.disconnect();
        }
      });

      const prom = waitForEvent(env.consumer, "close", 1, TIMEOUT * NB_IMAGES);

      env.consumer.connect();

      await prom;
      expect(received).to.be.equal((NB_IMAGES >> 1));
    });

  it("raises an error if the response content-type is not 'image/jpeg'",
    async function() {
      env.consumer = new HTTPConsumer({
        src: `http://localhost:${env.httpServer.address().port}/plain-text`
      });

      const prom = waitForEvent(env.consumer, "error");

      env.consumer.connect();

      const err = await prom;

      expect(err).to.be.an("error");
      expect(err.message).to.be.equal("No JPEG image detected");

    });
});
