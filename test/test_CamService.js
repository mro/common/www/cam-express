/* eslint-disable max-len */
/* eslint-disable max-lines */
// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)

import { expect } from "chai";
import url from "url";
import { afterEach, beforeEach, describe, it } from "mocha";
import axios from "axios";
import { replace, restore } from "sinon";
import { makeDeferred, set } from "@cern/nodash";
import WebSocket from "ws";
import { HTTPServer, MJPEGServer, waitFor,
  waitForEvent, waitForValue } from "./utils/index.js";
import config from "./config-stub.js";
import CamService from "../src/CamService.js";
import HTTPConsumer from "../src/consumers/HTTPConsumer.js";
import AppServer from "./utils/CamServer.js";
import MultiPart from "./MultiPart.js";

const { camApp, stubs } = config;

// test common config for CamService
function describeCameraService(name, opts, state, specificTests) {
  describe(name, function() {

    beforeEach(async function() {
      replace(CamService, "CONN_RETRY", 300);
      replace(HTTPConsumer, "DEFAULT_FRAME_THROTTLE", 500);

      if (opts.polling) {
        state.env.server = new HTTPServer(stubs);
      }
      else {
        state.env.server = new MJPEGServer(stubs);
      }

      await state.env.server.listen();

      // update camera config with target server port
      const port = state.env.server.address().port;

      const groups = Object.entries(camApp.groups).reduce(
        (ret, [ gName, group ]) => {
          set(ret, [ gName, "cameras" ], []);
          group.cameras?.forEach((cam) => {
            ret[gName].description = group?.description;
            ret[gName].cameras.push({
              name: cam.name,
              url: cam?.url ? `http://localhost:${port}${url.parse(cam.url, true).path}` : undefined,
              poll: opts.polling,
              throttle: opts.throttle,
              multiplex: cam.multiplex
            });
          });
          return ret;
        }, {});

      // app server
      state.env.appServer = new AppServer({
        basePath: "",
        camApp: {
          groups: groups,
          credentials: camApp.credentials
        }
      });

      await state.env.appServer.listen();
    });

    afterEach(function() {
      if (state.env.WScli?.readyState === WebSocket.OPEN) {
        state.env.WScli.close();
      }
      state.env.appServer?.close();
      state.env.server?.close();
      state.env = {};

      restore();
    });

    it("raises an error if a camera is not in the configuration",
      async function() {
        const appSrvPort = state.env.appServer.address().port;

        const group = "group1";
        const camera = "cam3";

        // using HTTP Get request
        await axios.get(
          `http://localhost:${appSrvPort}/cameras/${group}/${camera}`
        ).then(() => expect.fail("Should fail"))
        .catch((err) => {
          expect(err?.response?.status).to.be.equal(500);
          expect(err?.response?.data).to.contain("Camera not found");
        });

        // using WebSocket
        let errCode = 0;
        let errMsg = "";

        state.env.WScli = new WebSocket(
          `ws://localhost:${appSrvPort}/cameras/${group}/${camera}`
        );

        state.env.WScli.once("close", (code, reason) => {
          errCode = code;
          errMsg = reason?.toString();
        });

        return waitFor(
          () => (errCode === 1011 && errMsg === "Camera not found"));
      }
    );

    this.timeout(10000); // Avoid timeout when waiting for several frames

    // Injects the specific tests
    specificTests.map((test) => {
      it(test.name, test.func);
    });

  });
}

const state = { env: {} };

// Test specific config for MJPEGStream
describeCameraService("Cam Service MJPEGStream", { polling: false }, state, [
  {
    name: "can serve requests over HTTP",
    func: async function() {
      const appSrvPort = state.env.appServer.address().port;
      const group = "group1";
      const camera = "cam1";
      const streamer = state.env.server.streamers[camera];

      const jpegTest = Buffer.from("This is a test JPEG image");
      const subscription = waitForEvent(streamer, "subscribed");

      const res = await axios.get(`http://localhost:${appSrvPort}/cameras/${group}/${camera}`,
        { responseType: "stream" });

      const defer = makeDeferred();

      const part = (new MultiPart(res))
      .on("part", (contentType, data) => {
        if (contentType === "image/jpeg") {
          /* stop request */
          res.data.destroy();
          defer.resolve(data?.toString() ?? data);
        }
      });

      // send a frame from the MJPEG Streamer
      await subscription;
      streamer.sendFrame(jpegTest);

      const frame = await defer.promise;
      expect(frame).to.equal(jpegTest.toString());

      await part.promise;
    }
  },
  {
    name: "can serve requests over WebSocket (using Content-Length)",
    func: async () => {
      const appSrvPort = state.env.appServer.address().port;
      const group = "group1";
      const camera = "cam1";
      const streamer = state.env.server.streamers[camera];
      const expectedFrame = "This is a test JPEG image";
      const jpegTest = Buffer.from(expectedFrame);

      let buff = "";
      const subscription = waitForEvent(streamer, "subscribed");

      state.env.WScli = new WebSocket(
        `ws://localhost:${appSrvPort}/cameras/${group}/${camera}`
      );

      state.env.WScli.once("message", (data) => { buff = data?.toString(); });

      // send a frame from the MJPEG Streamer
      await subscription;
      streamer.sendFrame(jpegTest);

      return waitForValue(() => buff, expectedFrame);
    }
  },
  {
    name: "can serve requests over WebSocket (using boundary)",
    func: async () => {
      const appSrvPort = state.env.appServer.address().port;
      const group = "group1";
      const camera = "cam1";
      const streamer = state.env.server.streamers[camera];
      const expectedFrame = "This is a test JPEG image";
      const jpegTest = Buffer.from(expectedFrame);

      let buff = "";
      const subscription = waitForEvent(streamer, "subscribed");

      state.env.WScli = new WebSocket(
        `ws://localhost:${appSrvPort}/cameras/${group}/${camera}`
      );

      state.env.WScli.once("message", (data) => { buff = data?.toString(); });

      // send a frame from the MJPEG Streamer
      await subscription;
      streamer.sendFrame(jpegTest, false);

      const prom = waitFor(() => buff.includes(expectedFrame));

      // send empty frame to get next boundary delimiter line
      streamer.sendFrame(Buffer.from(""), false);

      return prom;
    }
  },
  {
    name: "can send errors without closing the WebSocket",
    func: async () => {
      const appSrvPort = state.env.appServer.address().port;
      const group = "group1";
      const camera = "cam1";
      const streamer = state.env.server.streamers[camera];

      streamer.boundary = ""; // cause a parsing error

      const unSubscription = waitForEvent(streamer, "unsubscribed", 1, 1500);

      // connect (by default 'closeOnError' is set to true)
      state.env.WScli = new WebSocket(
        `ws://localhost:${appSrvPort}/cameras/${group}/${camera}`
      );

      const closeCode = await waitForEvent(state.env.WScli, "close");
      expect(closeCode).to.be.equal(1011);

      await unSubscription;

      // re-connect (with 'closeOnError' set to false)
      state.env.WScli = new WebSocket(
        `ws://localhost:${appSrvPort}/cameras/${group}/${camera}?closeOnError=false`
      );

      const msg = waitForEvent(state.env.WScli, "message", 1, 1500);
      expect((await msg)?.toString()).to.include("error");
    }
  },
  {
    name: "can automatically reconnect to a camera",
    func: async () => {
      const appSrvPort = state.env.appServer.address().port;
      const group = "group1";
      const camera = "cam1";
      const streamer = state.env.server.streamers[camera];

      let subscription = waitForEvent(streamer, "subscribed");
      state.env.WScli = new WebSocket(
        `ws://localhost:${appSrvPort}/cameras/${group}/${camera}?closeOnError=false`
      );
      await subscription;

      const unSubscription = waitForEvent(streamer, "unsubscribed");
      subscription = waitForEvent(streamer, "subscribed");
      streamer.stop();
      await unSubscription;

      await subscription; // re-connected
    }
  }
]);

// Test specific config for HTTPPolling
describeCameraService("Cam Service HTTPPolling", { polling: true }, state, [
  {
    name: "can serve requests over WebSocket",
    func: async () => {
      const appSrvPort = state.env.appServer.address().port;
      const group = "group1";
      const camera = "cam2";
      let received = "";

      state.env.WScli = new WebSocket(
        `ws://localhost:${appSrvPort}/cameras/${group}/${camera}`
      );

      state.env.WScli.once("message", (data) => { received = data?.toString(); });

      return waitFor(() => received.includes("IMAGE_BUFFER"));
    }
  },
  {
    name: "can serve requests over HTTP",
    func: async () => {
      const appSrvPort = state.env.appServer.address().port;
      const group = "group1";
      const camera = "cam2";

      const res = await axios.get(`http://localhost:${appSrvPort}/cameras/${group}/${camera}`,
        { responseType: "stream" });

      const defer = makeDeferred();

      await (new MultiPart(res))
      .on("part", (contentType, data) => {
        if (contentType === "image/jpeg") {
          /* stop request */
          res.data.destroy();
          defer.resolve(data?.toString());
        }
      }).promise;

      expect(await defer.promise).to.equal("IMAGE_BUFFER");
    }
  },
  {
    name: "can send errors closing the HTTP connection",
    // eslint-disable-next-line max-statements
    func: async () => {
      const appSrvPort = state.env.appServer.address().port;
      const group = "group1";
      const camera = "cam2";
      const streamer = state.env.server.ressources[camera];

      const msg = makeDeferred();
      const closed = makeDeferred();
      const img = makeDeferred();

      // connect (by default 'closeOnError' is set to true)
      const res = await axios.get(`http://localhost:${appSrvPort}/cameras/${group}/${camera}`,
        { responseType: "stream" });

      const multipart = new MultiPart(res)
      .on("part", (contentType, data) => {
        if (contentType === "image/jpeg" && img.isPending) {
          img.resolve(data);
        }
        else if (contentType === "application/json" && msg.isPending) {
          msg.resolve(data);
        }
      });

      res.data
      .once("close", () => closed.resolve(null))
      .once("error", (err) => expect.fail(err.message));

      await img.promise;

      streamer.stop(); // stop polling
      expect((await msg.promise)?.toString()).to.include("error");
      await closed.promise;
      await multipart.promise;
    }
  },
  {
    name: "can send errors without closing the HTTP connection",
    func: async () => {
      const appSrvPort = state.env.appServer.address().port;
      const group = "group1";
      const camera = "cam2";
      const streamer = state.env.server.ressources[camera];

      const msg = makeDeferred();
      const closed = makeDeferred();
      let img = makeDeferred();

      const res = await axios.get(`http://localhost:${appSrvPort}/cameras/${group}/${camera}?closeOnError=false`,
        { responseType: "stream" });

      const multipart = new MultiPart(res)
      .on("part", (contentType, data) => {
        if (contentType === "image/jpeg" && img.isPending) {
          img.resolve(data);
        }
        else if (contentType === "application/json" && msg.isPending) {
          msg.resolve(data);
        }
      });

      res.data
      .once("close", () => closed.resolve(null))
      .once("error", (err) => expect.fail(err.message));

      await img.promise;

      streamer.stop(); // stop polling
      expect((await msg.promise)?.toString()).to.include("error");
      // should auto-reconnect
      img = makeDeferred(); // check that stream continues
      await img.promise;

      expect(closed.isPending).to.equal(true);

      res.data.destroy(); // force-close
      await multipart.promise;
      await closed.promise;
    }
  },
  {
    name: "can send errors without closing the WebSocket",
    func: async () => {
      const appSrvPort = state.env.appServer.address().port;
      const group = "group1";
      const camera = "cam2";
      const streamer = state.env.server.ressources[camera];

      // connect (by default 'closeOnError' is set to true)
      state.env.WScli = new WebSocket(
        `ws://localhost:${appSrvPort}/cameras/${group}/${camera}`
      );
      await waitForEvent(state.env.WScli, "message", 1, 1500);

      const wsClose = waitForEvent(state.env.WScli, "close");
      streamer.stop(); // stop stream
      await wsClose;

      // re-connect (with 'closeOnError' set to false)
      state.env.WScli = new WebSocket(
        `ws://localhost:${appSrvPort}/cameras/${group}/${camera}?closeOnError=false`
      );
      await waitForEvent(state.env.WScli, "message");

      const msg = waitForEvent(state.env.WScli, "message");
      streamer.stop(); // stop stream
      expect((await msg)?.toString()).to.include("error");

      await waitForEvent(state.env.WScli, "message");
    }
  },
  {
    name: "can automatically reconnect to a camera in HTTP",
    func: async () => {
      const appSrvPort = state.env.appServer.address().port;
      const group = "group1";
      const camera = "cam2";
      const streamer = state.env.server.ressources[camera];

      const msg = makeDeferred();
      const closed = makeDeferred();
      let img = makeDeferred();

      // connect (by default 'closeOnError' is set to true)
      const res = await axios.get(`http://localhost:${appSrvPort}/cameras/${group}/${camera}?closeOnError=false`,
        { responseType: "stream" });

      const multipart = new MultiPart(res)
      .on("part", (contentType, data) => {
        if (contentType === "image/jpeg" && img.isPending) {
          img.resolve(data);
        }
        else if (contentType === "application/json" && msg.isPending) {
          msg.resolve(data);
        }
      });

      res.data
      .once("close", () => closed.resolve(null))
      .once("error", (err) => expect.fail(err.message));

      await img.promise;
      streamer.stop(); // stop polling
      expect((await msg.promise)?.toString()).to.include("error");
      img = makeDeferred();
      await img.promise;

      res.data.destroy();
      await multipart.promise;
      await closed.promise;
    }
  },
  {
    name: "can automatically reconnect to a camera in WebSocket",
    func: async () => {
      const appSrvPort = state.env.appServer.address().port;
      const group = "group1";
      const camera = "cam2";
      const streamer = state.env.server.ressources[camera];

      const msg = makeDeferred();
      let img = makeDeferred();

      state.env.WScli = new WebSocket(
        `ws://localhost:${appSrvPort}/cameras/${group}/${camera}?closeOnError=false`
      );
      state.env.WScli.on("message", (data) => {
        const str = data?.toString();
        if (str === "IMAGE_BUFFER" && img.isPending) {
          img.resolve(data);
        }
        else if (str.includes("error") && msg.isPending) {
          msg.resolve(str);
        }
      });

      await img.promise;
      streamer.stop();
      await msg.promise;

      img = makeDeferred();
      await img.promise;
    }
  }
]);

/**
 * Compute the theorical duration of frames based on the client and the api throttle.
 * (For a better understanding, look at how the SteamClient throttling is handled.)
 *
 * @input {number} apiThrottle the throttle specified in the api (true throttle)
 * @input {number} cliThrottle the throttle specified in the cam (filtered from api)
 * @input {number} nbDurations the number of durations to be computed
 * @return {number[]} the durations in ms
*/
function theoricalFramesThrottle(apiThrottle, cliThrottle, nbDurations) {
  const indexes = [ ...Array(nbDurations).keys() ];
  let lastDiff = 0;

  return indexes.map(() => {
    const value =
      Math.ceil((cliThrottle - lastDiff) / apiThrottle) * apiThrottle;
    lastDiff += value - cliThrottle;
    return value;
  });
}

// Test specific config for MJPEGStream with throttling
describeCameraService("Cam Service MJPEGStream with throttling", { polling: false, throttle: 200 }, state, [
  {
    name: "can reduce frame rate from client",
    func: async () => {
      const appSrvPort = state.env.appServer.address().port;
      const group = "group1";
      const camera = "cam1";

      // throttle settings
      const timestamps = [];
      const cliThrottle = 400; // for easy case, cli % api = 0
      const srcThrottle = 50;

      const streamer = state.env.server.streamers[camera];
      const subscription = waitForEvent(streamer, "subscribed");

      state.env.WScli = new WebSocket(
        `ws://localhost:${appSrvPort}/cameras/${group}/${camera}` +
        `?throttle=${cliThrottle}`
      );

      state.env.WScli.on("message", () => timestamps.push(Date.now()));

      // send frames from the MJPEG Streamer
      await subscription;

      streamer.sendFrame(Buffer.from("frame"));
      const intervalHandler = setInterval(() => {
        streamer.sendFrame(Buffer.from("frame"));
      }, srcThrottle);

      await waitForEvent(state.env.WScli, "message", 2, 2000);

      clearInterval(intervalHandler);

      expect(timestamps[1] - timestamps[0])
      .to.be.approximately(cliThrottle, 10);
    }
  },
  {
    name: "can predict frame rate from client",
    func: async () => {
      const appSrvPort = state.env.appServer.address().port;
      const group = "group1";
      const camera = "cam1";

      // throttle settings
      const timestamps = [];
      const apiThrottle = 200;
      const cliThrottle = 500;
      const srcThrottle = 50;
      const nbFrames = 10;

      const streamer = state.env.server.streamers[camera];
      const subscription = waitForEvent(streamer, "subscribed");

      state.env.WScli = new WebSocket(
        `ws://localhost:${appSrvPort}/cameras/${group}/${camera}` +
        `?throttle=${cliThrottle}`
      );

      state.env.WScli.on("message", () => timestamps.push(Date.now()));

      // send frames from the MJPEG Streamer
      await subscription;

      const wait = waitForEvent(state.env.WScli, "message", nbFrames, (nbFrames + 1) * cliThrottle);

      streamer.sendFrame(Buffer.from("frame"));
      const intervalHandler = setInterval(() => {
        streamer.sendFrame(Buffer.from("frame"));
      }, srcThrottle);

      await wait;

      clearInterval(intervalHandler);

      // The client side throttle behaviour heavely depends on the one defined in the api
      // the only case where the client throttle will absolutely be respected is when
      //
      //                  `api <= cli` and `cli mod api = 0`.
      //
      // To evaluate if the cli throttle is still normally applyed, we can rely on
      // the theoricalFramesThrottle() defined above.

      const wanted =
        theoricalFramesThrottle(apiThrottle, cliThrottle, (nbFrames - 1));
      const durations = timestamps.map(
        (e, i) => timestamps?.[i + 1] - timestamps[i]);
      durations.pop(); // no delay for last frame

      const sumWanted = wanted.reduce((acc, val) => acc + val, 0);
      const sumDurations = durations.reduce((acc, val) => acc + val, 0);

      // We cannot compare frame by frame between what is expected and the reality as such:
      //
      // durations.forEach((e, i) => { expect(e).to.be.approximately(wanted[i], 10); });
      //
      // because there is a probability that a frame get late and that two durations are
      // swapped. What is more effective is to compare the number of frames that should
      // arrive in a duration. To do such, we can use the prediction made before and just
      // sum it to get the total expected duration to receive N frames.

      expect(sumDurations).to.be.approximately(sumWanted, nbFrames * 10);

    }
  }
]);

// Test specific config for HTTPPolling with throttling
describeCameraService("Cam Service HTTPPolling with throttling", { polling: true, throttle: 500 }, state, [
  {
    name: "can reduce frame rate from client",
    func: async () => {
      const appSrvPort = state.env.appServer.address().port;
      const group = "group1";
      const camera = "cam2";

      // throttle settings
      const timestamps = [];
      const cliThrottle = 1000; // for easy case, cli % api = 0

      state.env.WScli = new WebSocket(
        `ws://localhost:${appSrvPort}/cameras/${group}/${camera}` +
        `?throttle=${cliThrottle}`
      );
      state.env.WScli.on("message", () => timestamps.push(Date.now()));
      await waitForEvent(state.env.WScli, "message", 2, 3000);

      expect(timestamps[1] - timestamps[0])
      .to.be.approximately(cliThrottle, 10);
    }
  },
  {
    name: "can predict frame rate from client",
    func: async () => {
      const appSrvPort = state.env.appServer.address().port;
      const group = "group1";
      const camera = "cam2";

      // throttle settings
      const timestamps = [];
      const apiThrottle = 500;
      const cliThrottle = 700;
      const nbFrames = 10;

      state.env.WScli = new WebSocket(
        `ws://localhost:${appSrvPort}/cameras/${group}/${camera}` +
        `?throttle=${cliThrottle}`
      );
      state.env.WScli.on("message", () => timestamps.push(Date.now()));
      await waitForEvent(state.env.WScli, "message", nbFrames, (nbFrames + 1) * cliThrottle);

      // The client side throttle behaviour heavely depends on the one defined in the api
      // the only case where the client throttle will absolutely be respected is when
      //
      //                  `api <= cli` and `cli mod api = 0`.
      //
      // To evaluate if the cli throttle is still normally applyed, we can rely on
      // the theoricalFramesThrottle() defined above.

      const wanted =
        theoricalFramesThrottle(apiThrottle, cliThrottle, (nbFrames - 1));
      const durations =
        timestamps.map((e, i) => timestamps?.[i + 1] - timestamps[i]);
      durations.pop(); // no delay for last frame

      const sumWanted = wanted.reduce((acc, val) => acc + val, 0);
      const sumDurations = durations.reduce((acc, val) => acc + val, 0);

      // We cannot compare frame by frame between what is expected and the reality as such:
      //
      // durations.forEach((e, i) => { expect(e).to.be.approximately(wanted[i], 10); });
      //
      // because there is a probability that a frame get late and that two durations are
      // swapped. What is more effective is to compare the number of frames that should
      // arrive in a duration. To do such, we can use the prediction made before and just
      // sum it to get the total expected duration to receive N frames.

      expect(sumDurations).to.be.approximately(sumWanted, nbFrames * 10);
    }
  }
]);


// Test specific config for cameras multiplexing
describeCameraService("Cam Service Multiplexing", { polling: false, throttle: 200 }, state, [
  {
    name: "all cameras multiplexed",
    func: async () => {

      // These test only monitor 1 frame by camera, we have to manualy add delay
      // because the throttle is not preserved between cameras (when connected
      // the cameras will always instantly send their first frame). Because we only
      // check if we should rotate between cameras on frame sending, the rotation
      // duration will never be reached at the time the frame is sent.

      const DURATIONS = 100; // registered duration in the config

      // virtual delay (must be > than durations, but < than 2 * durations
      // for max duration, to avoid the force loop when camera stale is detected)
      const ADDED_DELAY = DURATIONS + 10;

      const appSrvPort = state.env.appServer.address().port;

      const config = state.env.appServer.config;
      const streamers = [];
      const messages = [];

      // Get streamers for each multiplexed cam
      config.camApp.groups["channels1"].cameras.cam5.multiplex.forEach((cam) => {
        streamers.push({
          path: `${cam.group}/${cam.camera}`,
          streamer: state.env.server.streamers[cam.camera]
        });
      });

      // Send a single frame when a streamer is connected to
      streamers.forEach((reference) => {
        waitForEvent(reference.streamer, "subscribed", 1, 3000)
        .then(() => {
          setTimeout(() => {
            reference.streamer.sendFrame(Buffer.from(reference.path));
          }, ADDED_DELAY); // needed delay
        });
      });

      state.env.WScli = new WebSocket(
        `ws://localhost:${appSrvPort}/cameras/channels1/cam5`
      );

      state.env.WScli.on("message", (msg) => {
        messages.push(msg?.toString());
      });

      await waitForEvent(state.env.WScli, "message", streamers.length, 3000);

      messages.forEach((msg, index) => {
        expect(msg).to.be.equal(streamers[index].path);
      });

    }
  },
  {
    name: "query declared multiplex",
    func: async () => {

      // These test only monitor 1 frame by camera, we have to manualy add delay
      // because the throttle is not preserved between cameras (when connected
      // the cameras will always instantly send their first frame). Because we only
      // check if we should rotate between cameras on frame sending, the rotation
      // duration will never be reached at the time the frame is sent.

      const DURATIONS = 100; // registered duration in the config

      // virtual delay (must be > than durations, but < than 2 * durations
      // for max duration, to avoid the force loop when camera stale is detected)
      const ADDED_DELAY = DURATIONS + 10;

      const appSrvPort = state.env.appServer.address().port;

      const config = state.env.appServer.config;
      const HTTPMsgs = [], WSMsgs = [];

      // Get streamers for each multiplexed cam
      const streamers = Object.values(config.camApp.groups["group1"].cameras ?? {})
      .map((cam) => ({
        path: `group1/${cam.name}`,
        streamer: state.env.server.streamers[cam.name]
      }));

      // Send a single frame when a streamer is connected to
      streamers.forEach((reference) => {
        waitForEvent(reference.streamer, "subscribed", 1, 2000)
        .then(() => {
          // needed delay
          setTimeout(() => {
            reference.streamer.sendFrame(Buffer.from(reference.path));
          }, ADDED_DELAY);
        });
      });

      const query =
        `cameras=${streamers.map((reference) => reference.path).join(",")}&` +
        `durations=${streamers.map(() => DURATIONS).join(",")}`;

      // using HTTP Get request
      const res = await axios.get(`http://localhost:${appSrvPort}/multiplex?${query}`,
        { responseType: "stream" });

      (new MultiPart(res))
      .on("part", (contentType, data) => {
        if (contentType === "image/jpeg") {
          HTTPMsgs.push(data?.toString());
        }
      });

      res.data
      .on("error", (err) => expect.fail(err.message));

      // using WebSocket
      state.env.WScli = new WebSocket(
        `ws://localhost:${appSrvPort}/multiplex?${query}`);

      state.env.WScli.on("message", (msg) => WSMsgs.push(msg?.toString()));

      await waitForValue(() => {
        return (HTTPMsgs.length === streamers.length) &&
          (WSMsgs.length === streamers.length);
      }, true, "Expected the array lengths to be equal", 2000);
      expect(HTTPMsgs).to.be.deep.equal(WSMsgs);

      HTTPMsgs.forEach((msg, index) => {
        expect(msg).to.be.equal(streamers[index].path);
      });
    }
  },
  {
    name: "pass stale multiplexed camera",
    func: async () => {

      const DURATIONS = 100; // registered duration in the config

      const NORMAL_ADDED_DELAY = DURATIONS + 10; // cameras will send at normal rate
      const STALE_ADDED_DELAY = DURATIONS + 5000; // camera will never send frame

      const STALE_CAMERA_INDEX = 1; // second camera is blocked and will not respond

      const appSrvPort = state.env.appServer.address().port;

      const config = state.env.appServer.config;
      const streamers = [];
      const messages = [];

      // Get streamers for each multiplexed cam
      config.camApp.groups["channels1"].cameras.cam5.multiplex.forEach((cam) => {
        streamers.push({
          path: `${cam.group}/${cam.camera}`,
          streamer: state.env.server.streamers[cam.camera]
        });
      });

      // Send a single frame when a streamer is connected to
      streamers.forEach(async (reference, index) => {
        waitForEvent(reference.streamer, "subscribed", 1, 3000)
        .then(() => {
          setTimeout(() => {
            reference.streamer.sendFrame(Buffer.from(reference.path));
          }, index === STALE_CAMERA_INDEX ?
            STALE_ADDED_DELAY : NORMAL_ADDED_DELAY); // needed delay
        });
      });

      state.env.WScli = new WebSocket(
        `ws://localhost:${appSrvPort}/cameras/channels1/cam5?closeOnError=0`
      );

      state.env.WScli.on("message", (msg) => {
        const m = msg?.toString();
        if (!m.startsWith("{")) { messages.push(m); }
      });

      await waitForEvent(state.env.WScli, "message", streamers.length - 1, 3000);

      // We store expected results and pop the stale camera one
      const expected = streamers.map((streamer) => streamer.path);
      expected.splice(STALE_CAMERA_INDEX, 1);

      messages.forEach((msg, index) => {

        // we shouldnt receive the stalled index
        if (index !== STALE_CAMERA_INDEX) {
          expect(msg).to.be.equal(expected[index]);
        }

      });
    }
  },
  {
    name: "raises an error with an incorrect query",
    func: async () => {
      const appSrvPort = state.env.appServer.address().port;

      // using HTTP Get request
      await axios.get(
        `http://localhost:${appSrvPort}/multiplex?cameras=XXXX`
      ).then(() => expect.fail("Should fail"))
      .catch((err) => {
        expect(err?.response?.status).to.be.equal(500);
        expect(err?.response?.data).to.contain("Bad query");
      });

      // using WebSocket
      state.env.WScli = new WebSocket(
        `ws://localhost:${appSrvPort}/multiplex?cameras=XXXX`);

      let errCode = 0;
      let errMsg = "";

      state.env.WScli.once("close", (code, reason) => {
        errCode = code;
        errMsg = reason.toString();
      });

      return waitFor(
        () => (errCode === 1011 && errMsg.includes("Bad query")));
    }
  }
]);
