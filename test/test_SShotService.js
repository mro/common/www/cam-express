// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)

import { expect } from "chai";
import axios from "axios";
import { afterEach, beforeEach, describe, it } from "mocha";
import { HTTPServer } from "./utils/index.js";
import { CamService } from "../src/index.js";

describe("Screenshot", function() {
  let env = {};
  const stubs = {
    cam1: {
      path: "/image/test.jpeg",
      auth: { user: "USER1", password: "PASS1" }
    }
  };

  beforeEach(async function() {
    env.httpServer = new HTTPServer(stubs);
    await env.httpServer.listen();
  });

  afterEach(function() {
    env.httpServer?.close();
    env.camService?.release();
    env = {};
  });

  it("can retrieve a screenshot", async function() {
    const port = env.httpServer.address().port;
    env.camService = new CamService({
      groups: {
        group1: {
          description: "Group 1 cameras",
          cameras: [
            { name: "cam1", url: "xxx",
              screenshot: `http://localhost:${port}/image/test.jpeg` },
            { name: "cam2", url: "/mjpg/cam2.mjpg" }, // no-screenshot
            { name: "cam3", url: "xxx",
              screenshot: `http://localhost:${port}/image/test.jpeg` } // no-auth
          ]
        }
      },
      credentials: { cam1: { user: "USER1", password: "PASS1" } }
    });
    env.camService.register(env.httpServer.app);

    const baseurl = `http://localhost:${env.httpServer.address().port}/cameras`;

    let ret = await axios.get(baseurl + "/group1/cam1/screenshot");
    expect(ret.status).to.equal(200);
    expect(ret.data).to.equal("IMAGE_BUFFER");

    const errorCases = [
      { url: "/group1/cam2/screenshot", code: 500,
        msg: "no screenshot for camera" },
      { url: "/group1/cam42/screenshot", code: 404, msg: "no such camera" },
      { url: "/group1/cam3/screenshot", code: 401,
        msg: "internal error: Unauthorized" }
    ];
    for (const t of errorCases) {
      ret = await axios.get(baseurl + t.url)
      .then(
        () => { throw new Error("should have failed"); },
        (err) => err);
      expect(ret?.response?.status).to.equal(t.code);
      expect(ret?.response?.data).to.equal(t.msg);
    }
  });
});
