// @ts-check
console.warn("Loading stub configuration");

export default {
  basePath: "",
  camApp: /** @type {CamExpress.Config} */ ({
    groups: {
      group1: {
        description: "Group 1 cameras",
        cameras: [
          { name: "cam1", url: "/mjpg/cam1.mjpg",
            screenshot: "/image/screenshot.jpeg",
            ptz: { type: "Axis", url: "/ptz" } },
          { name: "cam2", url: "/mjpg/cam2.mjpg", poll: true }
        ]
      },
      group2: {
        description: "Group 2 cameras",
        cameras: [
          { name: "cam3", url: "/mjpg/cam3.mjpg" },
          { name: "cam4", url: "/mjpg/cam4.mjpg" }
        ]
      },
      channels1: {
        description: "For multiplexing tests",
        cameras: [
          {
            name: "cam5", multiplex: [
              { group: "group1", camera: "cam1", duration: 100 },
              { group: "group1", camera: "cam2", duration: 100 },
              { group: "group2", camera: "cam3", duration: 100 },
              { group: "group2", camera: "cam4", duration: 100 }
            ]
          }
        ]
      }
    },
    credentials: {
      cam1: { user: "USER1", password: "PASS1" },
      cam2: { user: "USER2", password: "PASS2" },
      cam3: { user: "USER3", password: "PASS3" },
      cam4: { user: "USER4", password: "PASS4" }
    }
  }),
  // MJPEG Camera stubs
  stubs: {
    cam1: {
      path: "/mjpg/cam1.mjpg",
      auth: { user: "USER1", password: "PASS1" }
    },
    cam2: {
      path: "/mjpg/cam2.mjpg",
      auth: { user: "USER2", password: "PASS2" }
    },
    cam3: {
      path: "/mjpg/cam3.mjpg",
      auth: { user: "USER3", password: "PASS3" }
    },
    cam4: {
      path: "/mjpg/cam4.mjpg",
      auth: { user: "USER4", password: "PASS4" }
    }
  }
};
