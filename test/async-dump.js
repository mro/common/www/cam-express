// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)

import { isEmpty } from "@cern/nodash";

import { after, before } from "mocha";
import { createHook } from "async_hooks";
import { stackTraceFilter } from "mocha/lib/utils.js";

const allResources = new Map();

try {
  var process = require("process"); /* eslint-disable-line global-require */
}
catch (e) { } /* eslint-disable-line no-empty */

// this will pull Mocha internals out of the stacks
const filterStack = stackTraceFilter();

var hook;

if (process?.["env.ASYNC_DUMP"] === "1") {

  before(function() {
    hook = createHook({
      init(asyncId, type, triggerAsyncId) {
        allResources.set(asyncId,
          { type, triggerAsyncId, stack: (new Error()).stack });
      },
      destroy(asyncId) {
        allResources.delete(asyncId);
      },
      promiseResolve(asyncId) { allResources.delete(asyncId); }
    }).enable();
  });

  after(function() {
    if (global.gc) { global.gc(); }
    hook.disable();
    if (!isEmpty(allResources)) {
      console.error("STUFF STILL IN THE EVENT LOOP:");
      allResources.forEach((value) => {
        console.error(`Type: ${value.stack[0]}${value.type}`);
        console.error(filterStack(value.stack));
        console.error("\n");
      });
    }
  });
}
