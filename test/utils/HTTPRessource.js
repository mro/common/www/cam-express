// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)

import basicAuth from "basic-auth";
import { isNil } from "@cern/nodash";

/**
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 * @typedef {import('express').IRouter} IRouter
 *
 * @typedef {{
 *  auth?: { user: string, password: string }
 *  path: string
 * }} Options
 */

class HTTPRessource {

  /**
   * @param {Options} options
   */
  constructor(options) {
    //  basic authentication
    this.user = options?.auth?.user;
    this.pass = options?.auth?.password;

    this.path = options?.path ?? "/image.jpeg";

    this._stop = false;
  }

  /**
   * @param {IRouter} router
   */
  register(router) {

    router.get(this.path, (req, res) => {
      req.on("error",
        (err) => console.error("[HTTPRessource] error:", err.message));

      if (this._stop) {
        this._stop = false;
        res.destroy();
        return;
      }

      const auth = basicAuth(req);

      if (this.user && this.pass) {
        if (isNil(auth)) {
          res.sendStatus(401);
          return;
        }
        else if (!this.checkCreds(auth.name, auth.pass)) {
          res.sendStatus(403);
          return;
        }
      }

      res
      .status(200)
      .set("Content-Type", "image/jpeg")
      .send("IMAGE_BUFFER");
    });
  }

  stop() {
    this._stop = true;
  }

  /**
   * @param {string} user
   * @param {string} pass
   * @return {boolean}
   */
  checkCreds(user, pass) {
    if (this.user === user && this.pass === pass) {
      return true;
    }
    return false;
  }
}

export default HTTPRessource;
