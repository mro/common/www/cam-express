
export = AppServer
export as namespace AppServer

declare namespace AppServer {
  interface Config {
    port?: number,
    basePath: string,
    noWebSocket?: boolean,
    camApp: {
      unsubscribeTimeout?: number, // seconds
      groups: { [group: string]: {
        description?: string
        cameras: Array<CamApp.Camera>
      } },
      credentials: CamApp.Credentials
    },
    stubs?: {
      [cam: string]: {
        path: string,
        auth?: { user: string, password: string }
      }
    }
  }

  namespace CamApp {
    interface Camera {
      name: string,
      url: string,
      contentLength?: boolean,
      boundary?: string,
      headerDelimiter?: string,
      jpegMagicBytes?: boolean,
      throttle?: number, // milliseconds
      poll?: boolean,
      multiplex?: Array<{
        group: string,
        camera: string,
        duration?: number
      }>
    }
    interface Credentials {
      [key: string]: {
        user: string,
        password: string
      }
    }
  }
}
