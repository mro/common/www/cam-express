// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)

import express from "express";
import path from "node:path";
import { makeDeferred } from "@cern/nodash";
import ews from "express-ws";

import { CamService } from "../../src/index.js";
import { fileURLToPath } from "node:url";

/**
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 * @typedef {import('express').NextFunction} NextFunction
 * @typedef {{
 *   camApp: CamExpress.Config
 *   basePath: string
 * }} Config
 */

const dirname = path.dirname(fileURLToPath(import.meta.url));

class Server {
  /**
   * @param {Config} config
   */
  constructor(config) {
    this.config = config;
    this.app = express();
    this.camService = null;
    this._prom = this.prepare(config);
  }

  /**
   * @param {Config} config
   */
  async prepare(config) {
    ews(this.app);

    this.router = express.Router();

    this.app.set("view engine", "pug");
    this.app.set("views", path.join(dirname, "views"));

    this.router.get("/", (req, res) => res.render("index", config));

    /* NOTE: declare your additional endpoints here */
    this.camService = new CamService(config.camApp);
    this.camService.register(this.router);

    this.app.use(config.basePath, this.router);

    // default route
    this.app.use(function(req, res, next) {
      next({ status: 404, message: "Not Found" });
    });

    // error handler
    this.app.use(function(
      /** @type {any} */ err,
      /** @type {Request} */ req,
      /** @type {Response} */res,
      /** @type {NextFunction} */ next) { /* eslint-disable-line */ /* jshint ignore:line */
      res.locals.message = err.message;
      res.locals.error = req.app.get("env") === "development" ? err : {};
      res.locals.status = err.status || 500;
      res.status(err.status || 500);
      res.render("error", config);
    });
  }

  close() {
    if (this.camService) {
      this.camService.release();
      this.camService = null;
    }
    if (this.server) {
      this.server.close();
      this.server = null;
    }
  }

  /**
   * @param {() => any} cb
   */
  async listen(cb) {
    await this._prom;
    const def = makeDeferred();
    /* we're called as a main, let's listen */
    // @ts-ignore
    var server = this.app.listen(this.config.port, () => {
      this.server = server;
      def.resolve(undefined);
      return cb?.();
    });
    return def.promise;
  }

  address() {
    return this.server?.address() ?? null;
  }
}

export default Server;
