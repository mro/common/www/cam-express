// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)

const DELTA = 10;

class StreamClient {

  /**
   * @param {{ throttle: number|undefined, cb: (arg: Buffer|Error) => void}} arg
   */
  constructor({ throttle, cb }) {

    /** @type {number|undefined} */
    this.throttle = Number(throttle);

    /** @type {(Buffer|Error) => void} */
    this.cb = cb;

    /** @type {number} */
    this.next = 0; // last received frame timestamp
  }

  /** @param {Buffer|Error} data */
  throttledCB(data) {

    if (!this.throttle) {
      this.cb(data);
      return;
    }

    // Otherwise, we check if the required time interval has passed
    if (this.next - DELTA < Date.now()) {

      /**
       * We initialize this.next at 0 to have no throttle before the first frame,
       * so on the first receival we will need to initialize it.
       */
      const uninitialized = this.next === 0;

      /**
       * We also absolutely want to avoid the this.next timer to get overdue again
       * and again if we have some frames lost because when we will finally receive
       * again, all the next frames will be accepted in a row, breaking the framerate.
       *
       * normal  | <----------> (2) <----------> (3) <-------
       *       [1]____|______|_____[2]_____|_____[3]_____|
       *                     2nd accepted       3rd accepted
       *
       * overdue | <----------> (2) <----------> (3) <-------
       *       [1]____?______?______?______?____[2]____[3]
       *                 missing frames     two accepted at once!
      */
      const overdue = this.next < Date.now() - this.throttle;

      if (overdue) {
        console.warn("The client throttling was overdue and was reset");
      }

      // In both cases, we want to reset the this.next as `now + throttle`
      if (uninitialized || overdue) {
        this.next = Date.now();
      }

      this.next += this.throttle;
      this.cb(data);
    }
  }
}

export default StreamClient;
