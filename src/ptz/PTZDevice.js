// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)
// SPDX-Created: 2024-02-05T16:28:20
// SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

/**
 * @typedef {{ user: string, password: string }} Authentication
 * @typedef {{ src: string, auth?: Authentication }} Options
 */

class PTZDevice {
  constructor(options) {
    this.src = options?.src;
    this.user = options?.auth?.user;
    this.pass = options?.auth?.password;
  }

  /**
   * @return {Promise<any>}
   */
  async getLimits() { throw new Error("not implemented"); }

  /** @return {Promise<any>} */
  async getPosition(_req, _res) { throw new Error("not implemented"); }

  /**
   * @param {any} _params
   * @return {Promise<any>}
   */
  async putPosition(_params) { throw new Error("not implemented"); }

  /**
   * @param {any} _params
   * @return {Promise<any>}
   */
  async postPosition(_params) { throw new Error("not implemented"); }
}

export default PTZDevice;
