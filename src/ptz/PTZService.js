// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)
// SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

import bodyParser from "body-parser";
import AxisPTZ from "./AxisPTZ.js";

/**
 * @typedef {import('./PTZDevice')} PTZDevice
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 *
 * @typedef {import('express').IRouter} expressIRouter
 * @typedef {import('express-ws').WithWebsocketMethod & expressIRouter} IRouter
 */

const registry = {
  axis: AxisPTZ
};

const JsonParser = bodyParser.json({ strict: false });

class PTZService {
  /**
   * @param {CamExpress.InternalGroupConfig} groups
   * @param {CamExpress.Credentials} credentials
   */
  constructor(groups, credentials) {
    this.groups = groups;
    this.creds = credentials;

    /** @type {{ [id: string]: PTZDevice }} */
    this._ptz = {};
  }

  /**
   * @param {IRouter} router
   */
  register(router) {
    /**
     * @swagger
     * /cameras/{group}/{camera}/ptz/limits:
     *   get:
     *     tags: ['ptz']
     *     summary: get PTZ limits configuration
     *     description: |
     *       Exposes to PTZ limits to clients
     *     parameters:
     *      - in: path
     *        name: group
     *        schema:
     *           type: string
     *        description: name of the group defined in the config
     *        required: true
     *      - in: path
     *        name: camera
     *        schema:
     *           type: string
     *        description: name of the camera defined in the config
     *        required: true
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: PTZ limits configuration
     *       500:
     *         description: error message
     */
    router.get("/cameras/:group/:camera/ptz/limits",
      this._call.bind(this, (ptz) => ptz.getLimits()));

    /**
     * @swagger
     * /cameras/{group}/{camera}/ptz:
     *   get:
     *     tags: ['ptz']
     *     summary: get PTZ current position
     *     description: |
     *       Exposes to clients PTZ position
     *     parameters:
     *      - in: path
     *        name: group
     *        schema:
     *           type: string
     *        description: name of the group defined in the config
     *        required: true
     *      - in: path
     *        name: camera
     *        schema:
     *           type: string
     *        description: name of the camera defined in the config
     *        required: true
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: PTZ limits position
     *       500:
     *         description: error message
     */
    router.get("/cameras/:group/:camera/ptz",
      this._call.bind(this, (ptz) => ptz.getPosition()));

    /**
     * @swagger
     * /cameras/{group}/{camera}/ptz:
     *   put:
     *     tags: ['ptz']
     *     summary: execute an absolute PTZ motion
     *     description: |
     *       Sends an absolute motion command to PTZ
     *     parameters:
     *      - in: path
     *        name: group
     *        schema:
     *           type: string
     *        description: name of the group defined in the config
     *        required: true
     *      - in: path
     *        name: camera
     *        schema:
     *           type: string
     *        description: name of the camera defined in the config
     *        required: true
     *      - in: body
     *        name: motion parameters
     *        description: 'PTZ absolute motion values'
     *        required: true
     *        example:
     *          pan: 0
     *          tilt: 0
     *          zoom: 200
     *          iris: 6248
     *          focus: 9854
     *          brightness: 4999
     *          autofocus: true
     *          autoiris: true
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: PTZ absolute movement ongoing
     *       500:
     *         description: error message
     */
    router.put("/cameras/:group/:camera/ptz", JsonParser,
      this._call.bind(this, (ptz, req) => ptz.putPosition(req.body)));

    /**
     * @swagger
     * /cameras/{group}/{camera}/ptz:
     *   post:
     *     tags: ['ptz']
     *     summary: execute an relative PTZ motion
     *     description: |
     *       Sends a relative motion command to PTZ
     *     parameters:
     *      - in: path
     *        name: group
     *        schema:
     *           type: string
     *        description: name of the group defined in the config
     *        required: true
     *      - in: path
     *        name: camera
     *        schema:
     *           type: string
     *        description: name of the camera defined in the config
     *        required: true
     *      - in: body
     *        name: motion parameters
     *        description: 'PTZ relative motion values'
     *        required: true
     *        example:
     *          pan: -10
     *          tilt: -10
     *          zoom: -20
     *          iris: -10
     *          focus: -100
     *          brightness: -10
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: PTZ relative movement ongoing
     *       500:
     *         description: error message
     */
    router.post("/cameras/:group/:camera/ptz", JsonParser,
      this._call.bind(this, (ptz, req) => ptz.postPosition(req.body)));
  }

  /**
   * @param {Request} req
   * @param {Response} res
   * @param {(ptz: PTZDevice, req: Request, res: Response) => Promise<any>} cb
   * @returns
   */
  async _call(cb, req, res) {
    const ptz = this.getPTZ(this.getCamera(req));
    if (ptz) {
      try {
        return res.json(await cb(ptz, req, res));
      }
      catch (err) {
        return res.status(500).send(err?.message ?? "internal error");
      }
    }
    else {
      return res.status(404).send("PTZ Camera not found");
    }
  }

  /**
   * @param {Request} req
   * @return {CamExpress.CameraConfig&{id:string|number}|undefined}
   */
  getCamera(req) {
    const group = req?.params?.group;
    const camera = req?.params?.camera;
    return this.groups?.[group]?.cameras?.[camera];
  }

  /**
   * @param {CamExpress.CameraConfig&{id:string|number}|undefined} cam
   * @return {PTZDevice|null}
   */
  getPTZ(cam) {
    if (!cam) { return null; }
    let ptz = this._ptz[cam.id];
    if (!ptz && cam.ptz) {
      const Ctrl = registry[(cam.ptz?.type ?? "").toLowerCase()];
      if (Ctrl) {
        ptz = new Ctrl({ src: cam.ptz.url,
          auth: this.creds?.[cam.name] });
        this._ptz[cam.id] = ptz;
      }
    }
    return ptz;
  }

  release() {
    this._ptz = {};
  }
}

export default PTZService;
