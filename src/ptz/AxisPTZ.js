// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)
// SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

import { isNil } from "@cern/nodash";
import d from "debug";
import axios from "axios";
import PTZDevice from "./PTZDevice.js";

const debug = d("cam:ptz:axis");

/**
 * @typedef {import('express').Request} Request
 * @typedef {import('express').IRouter} expressIRouter
 * @typedef {import('express-ws').WithWebsocketMethod & expressIRouter} IRouter
 */

const ControlParams = [ "pan", "tilt", "zoom", "iris", "focus", "brightness" ];
const ExtraControlParams = [
  "backlight", "ircutfilter", "imagerotation", "speed", "gotodevicepreset",
  "gotopresetname", "auxiliary", "continuousbrightnessmove",
  "continuousirismove", "continuousfocusmove", "continuouszoommove",
  "continuouspantiltmove", "autoiris", "autofocus", "imageheight", "imagewidth",
  "areazoom", "center", "whoami", "camera"
];

class AxisPTZ extends PTZDevice {
  /**
   *
   * @param {any} data
   */
  _parseResponse(data) {
    return ((typeof data === "string") ? data : "").split("\n").reduce((ret, line) => {
      const pos = line.indexOf("=");
      if (pos >= 0) {
        ret[line.slice(0, pos).trim()] = line.slice(pos + 1).trim();
      }
      return ret;
    }, {});
  }

  async getLimits() {
    const opts = { timeout: 5000 };
    if (this.user) {
      opts.auth = { username: this.user, password: this.pass };
    }

    const ret = await axios.get(
      `${this.src}/axis-cgi/com/ptz.cgi?query=limits`, opts)
    .catch((ret) => {
      debug("get limits error: %s", ret?.response?.data ?? "?");
      throw new Error("request failed with status: " + ret?.response?.status);
    });
    return Object.entries(this._parseResponse(ret.data)).reduce(
      (ret, [ key, value ]) => {
        ret[key] = Number(value);
        return ret;
      }, {});
  }

  async getPosition() {
    const opts = { timeout: 5000 };
    if (this.user) {
      opts.auth = { username: this.user, password: this.pass };
    }

    const ret = await axios.get(
      `${this.src}/axis-cgi/com/ptz.cgi?query=position`, opts)
    .catch((ret) => {
      debug("get position error: %s", ret?.response?.data ?? "?");
      throw new Error("request failed with status: " + ret?.response?.status);
    });
    return Object.entries(this._parseResponse(ret.data)).reduce(
      (ret, [ key, v ]) => {
        if (key === "autofocus" || key === "autoiris") {
          ret[key] = (v === "on");
        }
        else {
          ret[key] = Number(v);
        }
        return ret;
      }, {});
  }

  /**
   * @param {any} params
   * @param {any} query
   */
  _prepareQuery(params, query) {
    (ExtraControlParams ?? []).forEach((p) => {
      const value = params[p];
      if (isNil(value)) { return; }
      else if (value === true || value === false) {
        query[p] = value ? "on" : "off";
      }
      else {
        query[p] = value;
      }
    });
  }

  async putPosition(params) {
    if (!(params instanceof Object)) { throw new Error("invalid arguments: " + params); }

    const opts = { timeout: 5000 };
    if (this.user) {
      opts.auth = { username: this.user, password: this.pass };
    }

    opts.params = ControlParams.reduce((ret, name) => {
      if (!isNil(params[name])) {
        ret[name] = params[name];
      }
      return ret;
    }, {});

    this._prepareQuery(params, opts.params);

    await axios.get(`${this.src}/axis-cgi/com/ptz.cgi`, opts)
    .catch((ret) => {
      debug("put position error: %s", ret?.response?.data ?? "?");
      throw new Error("request failed with status: " + ret?.response?.status);
    });
    return true;
  }

  async postPosition(params) {
    if (!(params instanceof Object)) { throw new Error("invalid arguments: " + params); }

    const opts = { timeout: 5000 };
    if (this.user) {
      opts.auth = { username: this.user, password: this.pass };
    }

    opts.params = ControlParams.reduce((ret, name) => {
      if (!isNil(params[name])) {
        ret["r" + name] = params[name];
      }
      return ret;
    }, {});
    this._prepareQuery(params, opts.params);
    await axios.get(`${this.src}/axis-cgi/com/ptz.cgi`, opts)
    .catch((ret) => {
      debug("post position error: %s", ret?.response?.data ?? "?");
      throw new Error("request failed with status: " + ret?.response?.status);
    });
    return true;
  }
}

export default AxisPTZ;
