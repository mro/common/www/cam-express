// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)

import path from "node:path";
import swaggerJSDoc from "swagger-jsdoc";
import swaggerUi from "swagger-ui-express";
import { cloneDeep, merge } from "@cern/nodash";
import { fileURLToPath } from "node:url";

/**
 * @typedef {import('express').Router} Router
 */

const dirname = path.dirname(fileURLToPath(import.meta.url));

class CamSwagger {

  /**
   * @param {CamExpress.CamSwagger.Options} [options]
   * @param {CamExpress.CamSwagger.Info} [extraDoc]
   */
  constructor(options, extraDoc) {

    // fill api defintion
    const definition = options?.definition ?? {
      info: {
        title: "cam-express",
        version: "1.0.0"
      }
    };

    /** @type {any} */
    this.doc = swaggerJSDoc({
      definition,
      apis: [
        path.join(dirname, "**/**.js"),
        path.join(dirname, "../src/**/**.js")
      ]
    });

    this.extra = extraDoc;
  }

  /**
   * @param {Router} router
   */
  register(router) {
    router.get("/api-docs.json", (req, res) => {
      res.json(this.doc);
    });

    const opts = {
      swaggerOptions: {
        docExpansion: "none",
        displayRequestDuration: true
      }
    };

    router.use("/api-docs",
      swaggerUi.serveFiles(this.doc, opts),
      swaggerUi.setup(this.doc, opts)
    );
  }

  /**
   * @brief generate swagger doc for given group and camera
   * @param {string} groupName
   * @param {CamExpress.CameraInfo} camera
   */
  camera(groupName, camera) {
    const path = `/cameras/${groupName}/${camera.name}`;
    const cameraDoc = cloneDeep(this.doc?.paths?.["/cameras/{group}/{camera}"]);

    if (!cameraDoc) {
      console.warn("Unable to find Swagger camera doc");
      return;
    }

    // remove path parameters to be filled
    cameraDoc["get"].parameters.splice(0, 2);

    // replace summary by camera description
    cameraDoc["get"].summary = camera.description ||
      "no description provided for this camera";


    if (camera.ptz) {
      [ "/ptz", "/ptz/limits" ].forEach(
        (sub) => {
          const template = "/cameras/{group}/{camera}" + sub;
          const doc = cloneDeep(this.doc?.paths?.[template]);
          // remove path parameters to be filled
          doc["get"].parameters.splice(0, 2);
          this.doc.paths[path + sub] = doc;
        });
    }
    if (camera.screenshot) {
      const template = "/cameras/{group}/{camera}/screenshot";
      const doc = cloneDeep(this.doc?.paths?.[template]);
      // remove path parameters to be filled
      doc["get"].parameters.splice(0, 2);
      this.doc.paths[path + "/screenshot"] = doc;
    }

    // replace var path with current cam path
    this.doc.paths[path] = cameraDoc;
  }

  /**
   * Add dynamic doc from cameras config
   * @param {CamExpress.CamService} service
   */
  swag(service) {
    let hasPTZ = false;
    let hasSShot = false;

    Object.entries(service.camConfig ?? {}).forEach(([ groupName, group ]) => {
      group.cameras?.forEach((camera) => {
        this.camera(groupName, camera);

        hasPTZ = hasPTZ || (camera?.ptz ?? false);
        hasSShot = hasSShot || (camera?.screenshot ?? false);
      });
    });
    if (!hasPTZ) {
      /* remove ptz documentation if not needed */
      [ "/ptz", "/ptz/limits" ].forEach(
        (sub) => { delete this.doc.paths["/cameras/{group}/{camera}" + sub]; });
    }
    if (!hasSShot) {
      /* remove ptz documentation if not needed */
      [ "/screenshot" ].forEach(
        (sub) => { delete this.doc.paths["/cameras/{group}/{camera}" + sub]; });
    }

    // Insert additional paths in doc
    merge(this.doc?.paths, this.extra?.paths);
  }
}

export default CamSwagger;
