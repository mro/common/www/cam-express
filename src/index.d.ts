
import { IRouter } from "express";
import { EventEmitter } from "events";

export = CamExpress;
export as namespace CamExpress;

declare namespace CamExpress {
  interface PTZConfig {
    type: string
    url: string
  }

  interface CameraConfig {
    name: string
    description?: string,
    url?: string
    contentLength?: boolean
    boundary?: string
    headerDelimiter?: string
    jpegMagicBytes?: boolean
    throttle?: number
    poll?: boolean
    multiplex?: Reference[]
    ptz?: PTZConfig,
    screenshot?: string
  }

  interface Camera extends CameraConfig {
    id: number
  }

  interface Reference {
    group: string,
    camera: string,
    duration?: number
  }

  interface Credentials {
    [key: string]: {
      user: string
      password: string
    }
  }

  interface CameraList {
    [group: string]: {
      description?: string,
      cameras: CameraInfo[]
    }
  }

  interface CameraInfo {
    name: string,
    description?: string,
    ptz?: boolean
    screenshot?: boolean
  }

  interface GroupConfig {
    description?: string
    cameras: CameraConfig[]
  }

  interface Config {
    unsubscribeTimeout?: number
    groups: {
      [group: string]: GroupConfig
    },
    credentials: Credentials
  }

  /* camera runtime information */
  interface CameraMap {
    [camId: string]: {
      clients: { [clientId: number]: StreamClient },
      consumer: Consumer|null,
      retryTimer: NodeJS.Timeout|null,
      retryAttempt: number,
      unSubTimeout: NodeJS.Timeout|null,
      error: Error|null
    }
  }

  interface Groups {
    [group: string]: Camera[]
  }

  class StreamClient extends EventEmitter {
    constructor()

    cb(data?: Buffer|Error): void

    throttle?: number;

    throttledCB(arg: Buffer|Error): void;
  }

  class CamService {
    constructor(
      config: Config,
      options?: CamSwagger.Options,
      extra?: CamSwagger.Info)

    groups: CamExpress.InternalGroupConfig;

    creds: CamExpress.Credentials;

    camConfig: CamExpress.CameraList;

    register(router: IRouter): void

    subscribe(
      cam: Camera,
      throttle: number | undefined,
      cb: (data?: Buffer | Error) => void
    ): number

    unsubscribe(cam: Camera, id: number): void

    release(): void
  }

  interface MJPEGConsumerOptions {
    src: string
    auth?: { user: string, password: string }
    contentLength?: boolean
    headerDelimiter?: string
    jpegMagicBytes?: boolean
    boundary?: string
    throttle?: number
  }

  interface HTTPConsumerOptions {
    src: string
    auth?: { user: string, password: string }
    throttle?: number
  }

  interface MultiplexConsumerOptions {
    service: CamService,
    cameras: Array<{ camera: Camera, duration?: number }>,
    index?: number
  }

  interface Consumer extends EventEmitter {
    connect(): void

    disconnect(): void

    emitError(): void

    emitFrame(): void
  }

  class ConsumerFactory {
    static createConsumer(cam: Camera, service: CamService): Consumer

    static createMultiplexConsumer(
      cam: Camera, service: CamService): MultiplexConsumer

    static createMJPEGConsumer(cam: Camera, creds: Credentials): MJPEGConsumer

    static createHTTPConsumer(cam: Camera, creds: Credentials): HTTPConsumer
  }

  class MJPEGConsumer extends EventEmitter implements Consumer {
    static MIN_THROTTLE_VALUE: number;

    static MAX_HEADER_LENGTH: number;

    static MAX_CONTENT_LENGTH: number;

    constructor(options: MJPEGConsumerOptions)

    connect(): void

    disconnect(): void

    emitError(): void

    emitFrame(): void

    updateThrottle(throttle: number): void

    src: string;

    user: string | undefined;

    pass: string | undefined;

    contentLength: boolean;

    headerDelimiter: string;

    jpegMagicBytes: boolean;

    boundary: string | undefined;

    throttle: number | undefined;
  }

  class HTTPConsumer extends EventEmitter implements Consumer {
    static MIN_THROTTLE_VALUE: number;

    constructor(options: HTTPConsumerOptions)

    connect(): void

    disconnect(): void

    emitError(): void

    emitFrame(): void

    updateThrottle(throttle: number): void

    src: string;

    user: string | undefined;

    pass: string | undefined;

    throttle: number | undefined;
  }

  class MultiplexConsumer extends EventEmitter implements Consumer {
    constructor(options: MultiplexConsumerOptions)

    connect(): void

    disconnect(): void

    emitError(): void

    emitFrame(): void

    updateThrottle(throttle: number): void
  }

  namespace CamSwagger {
    interface Options {
      definition?: {
        info: {
          title: string
          version: string
          description?: string
          termsOfService?: string
          contact?: { name?: string, url?: string, email?: string }
          license?: { name: string, url?: string }
        }
        host?: string
        // use this one to add prefix on complete API (when using a Router)
        basePath?: string
      }
    }

    interface Info {
      paths?: { [path: string]: any }
    }
  }

  interface InternalGroupConfig {
      [group: string]: {
        description: string,
        cameras: {[name: string]: CamExpress.Camera
      }
    }
  }
}
