// @ts-check
// SPDX-License-Identifier: Zlib
// SPDX-FileCopyrightText: 2024 CERN (home.cern)

import MultiplexConsumer from "./MultiplexConsumer.js";
import MJPEGConsumer from "./MJPEGConsumer.js";
import HTTPConsumer from "./HTTPConsumer.js";
import { isEmpty, isNil } from "@cern/nodash";

/**
 * @typedef {import('../index.d.ts')} CamExpress
 */

/**
 * @implements {CamExpress.ConsumerFactory}
 */
class ConsumerFactory {

  /**
   * @param {CamExpress.Camera} cam
   * @param {CamExpress.CamService} service
   * @return {CamExpress.Consumer}
   */
  static createConsumer(cam, service) {
    let consumer;

    // Determine the right type of consumer
    if (!isNil(cam.multiplex)) {
      consumer = ConsumerFactory.createMultiplexConsumer(cam, service);
    }
    else if (cam.poll) {
      consumer = ConsumerFactory.createHTTPConsumer(cam, service.creds);
    }
    else {
      consumer = ConsumerFactory.createMJPEGConsumer(cam, service.creds);
    }

    if (isNil(consumer)) {
      throw new Error("Consumer initialisation failed");
    }

    return consumer;

  }

  /**
   * @param {CamExpress.Camera} cam
   * @param {CamExpress.CamService} service
   * @return {CamExpress.MultiplexConsumer}
   */
  static createMultiplexConsumer(cam, service) {
    if (isEmpty(cam.multiplex)) {
      throw new Error("No camera in the multiplex channel");
    }

    const references = cam.multiplex?.map((ref) => {
      if (isNil(ref.group) || isNil(ref.camera)) {
        throw new Error("No given path for the camera");
      }

      const group = service.groups?.[ref.group]?.cameras ?? {};
      const camera = Object.values(group).find(
        (camera) => (camera?.name === ref.camera));

      if (camera === undefined) {
        throw new Error(`No camera found at ${ref.group}/${ref.camera}`);
      }

      const duration = ref.duration;

      return { camera, duration };
    }) ?? [];

    return new MultiplexConsumer({
      service: service,
      cameras: references,
      index: 0
    });
  }

  /**
   * @param {CamExpress.Camera} cam
   * @param {CamExpress.Credentials} creds
   * @return {CamExpress.MJPEGConsumer}
   */
  static createMJPEGConsumer(cam, creds) {
    return new MJPEGConsumer({
      src: cam.url,
      auth: creds?.[cam.name],
      contentLength: cam.contentLength,
      headerDelimiter: cam.headerDelimiter,
      boundary: cam.boundary,
      throttle: cam.throttle
    });
  }

  /**
   * @param {CamExpress.Camera} cam
   * @param {CamExpress.Credentials} creds
   * @return {HTTPConsumer}
   */
  static createHTTPConsumer(cam, creds) {

    return new HTTPConsumer({
      src: cam.url,
      auth: creds?.[cam.name],
      throttle: cam.throttle
    });
  }
}

export default ConsumerFactory;
