# MJPEG Camera Streamer

A Node.js library for streaming MJPEG cameras.

This module provides both a service that can serve MJPEG streams over HTTP and WebSocket and a simple client that extracts frames from an MJPEG stream.

## How it works

<div align="center">
  <img alt="cam-express chart" height="300px" src="data/cam-express_chart.svg">
</div>

The **cam-express service** exposes to clients two routes ([Express.js](https://expressjs.com/) endpoints) for each camera specified in the configuration allowing streaming via HTTP and WebSocket protocols. It only establishes a connection with the camera if there is at least one client requesting it.

The HTTP request handler merely re-transmits the stream fragments (frame header and content) received from the source, while the WebSocket handler sends the JPEG images extracted from the fragments.

The main features of cam-express service are:

- overcoming the limits on the number of HTTP connections a browser can maintain with the same server simultaneously ([RFC 2616](https://www.w3.org/Protocols/rfc2616/rfc2616-sec8.html#sec8.1.4)) thanks to streaming via WebSocket;
- broadcasting the same camera stream to multiple clients;
- relieving the client to know the credentials to access the MJPEG stream by configuring them on the server side.

The **cam-express client** (`MJPEG Consumer` or `HTTP Consumer`), on the other hand, can connect to a single camera and emit a JPEG image each time it receives one from the `source`.

<div align="center">
  <img alt="schemaGlobalApiFramerate" style="width:100%; max-width: 700px" src="data/schemaGlobalApiFramerate.svg">
  <p style="font-style: italic">Data communication within cam-express components and with external exposures</p>
</div>

### Framerate

The `throttle` (Time between two frames) can be specified both:

 * in the api cameras configs (`camera: { name: string, url: string, throttle?: number }`)

 * from the client with url query (`/cameras/<group>/<camera>?throttle=number`)

The behaviour of the CamExpress framework will then depend on how this parameter is provided. These behaviors are dynamic, and are determined at each connection/disconnection of clients.

<div align="center">
  <img alt="schemaGlobalClientFramerate" style="width:100%; max-width: 700px" src="data/schemaGlobalClientFramerate.svg">
  <p style="font-style: italic">Mechanisms of framerate determination</p>
</div>

#### 1. No throttle

If throttle is not specified neither from the api nor from the clients, the frame will be fetched at the maximum speed (see `DEFAULT_FRAME_THROTTLE` in the consumers) and instantly forwarded to the the clients with `broadcasting`.

#### 2. Api throttle

If the throttle is only specified in the api, the data fetching rate will be adapted to best fit the expected duration:

- The `MJPEGConsumer` will filter the received stream to send only one frame for a certain interval.
- The `HTTPConsumer` will send a new request at the specified rate, after making sure it has received the previous frame.

The frames will then be forwarded to the the clients with `broadcasting` when the received event is triggered.

#### 3. Client throttle

Each client can specify a custom throttle, or don't give any to expect the maximum possible rate (see *2. Api throttle)*. The rate at which the api should retrieve frames from the cameras is based on the api-specified throttle.

But for each receiving, we will compute the time elapsed since the last image and send the data to the clients for which the interval was greater than the specified throttling.

<div align="center">
  <img alt="cam-schedulling chart" style="width:100%;max-width: 700px" src="data/cam-schedulling_chart.svg">
  <p style="font-style: italic">Scheduling of frame retrieval from base frequency</p>
</div>

The client side throttle behaviour heavely depends on the one defined in the api.

- The only case where the client throttle will absolutely be respected is when `api <= cli` and `cli mod api = 0`.

- Otherwise, the framerate will converge to the theoretical value defined through the client throttle, but the time between frames will be irregular.

<div align="center">
  <img alt="schema for the client delays" style="width:100%;max-width: 700px" src="data/schemaClientDelays.svg">
  <p style="font-style: italic">Temporal diagram for an example of the client throttle variations</p>
</div>

### Multiplexing

#### 1. Via configuration

You can choose to receive multiple rotating camera streams on the same websocket connection by registering a multiplexed configuration.

To create it, specify a camera with only a `name` and a `multiplex` field. The second one is as list referencing already registered cameras with their duration as such: `{ group, camera, duration }` (see `channel1` in the exemple configuration below).

To access these multiplexed cameras, use the same url as strandard cameras (`cameras/:group/:camera`).

If the cameras duration are low, it is recommanded to specify a `unsubscribeTimeout` value in the configuration to avoid re-opening a new connexion with the cameras at each iteration.

#### 2. Via query

You can also dynamically create a new camera rotation and receive it by declaring it via a query at `/multiplex`. You will have to specify the cameras to rotate in the `cameras` query parameter, with the group the camera name, as such: `group/camera`. The durations are in the `durations` param. If for a camera the duration is not or badly specified, it will be replaced by the default value (1000ms).

*example:* `/multiplex?cameras=default/cam1,default/cam2&durations=2000,4000` will rotate between cam1 (2000ms) and cam2 (4000ms).

### Error handling

This section describes how errors are handled by the cam-express service. By default a connection gets closed if an error occurs including those from connections with cameras.

It is possible to forward errors throught WebSockets by declaring `closeOnError=false` in the query string of the camera URL , as shown in the following example:

`/cameras/<group>/<camera>?closeOnError=false`.

In such a case, one can distinguish a frame from an error checking at least the type of the [data](https://developer.mozilla.org/en-US/docs/Web/API/MessageEvent/data) sent on the WebSocket:
- _Blob_, an object containing a JPEG image which can be used as URL for HTML `<img>` tag (and others) via the [`URL.createObjectURL()`](https://developer.mozilla.org/en-US/docs/Web/API/URL/createObjectURL) method;
- _string_, a JSON string like `{ "type": "error", "data": "<message>" }`.

An additional check on the first byte(s) can make more sure about what was received. Namely, the [JPEG magic numbers](https://en.wikipedia.org/wiki/List_of_file_signatures) (`FF D8 FF`) in case of blobs and an opening curly brace (`{`) for strings.

## Install

```bash
# In your project:
npm install git+https://gitlab.cern.ch/mro/common/www/cam-express.git
```

## Usage

### MJPEG Camera service

To serve MJPEG stream through HTTP and/or WebSocket (acting as a proxy).

Once the service is registered on an Express.js app/router and the server is listening, the MJPEG streams can be reached at:
- `http(s)://<server>:<port>/cameras/<group>/<camera>` (over HTTP)
- `ws(s)://<server>:<port>/cameras/<group>/<camera>` (over WebSocket)

Below is the service configuration info:

```ts
interface Config {
  // idle timeout before unsubscribing from the camera MJPEG stream in seconds
  // (default: imminent unsubscription)
  unsubscribeTimeout?: number,
  // camera groups
  groups: {
    [ group: string ]: {
      description?: string,
      cameras: Array<{
        // camera name
        name: string,
        // camera description
        description?: string,
        // MJPEG stream URL of camera to play
        url?: string,
        // enable the use of 'Content-Length' header field when extracting frames from MJPEG stream (default: enabled)
        contentLength?: boolean
        // boundary delimiter used in MJPEG stream fragmentation (default: extracted from the header)
        boundary?: string, // (*)
        // string representing the separator between the header and the body of a frame (default: '\r\n\r\n')
        headerDelimiter?: string,
        // force frame detection using the JPEG magic numbers (default: false)
        jpegMagicBytes?: boolean,
        // frames throttle in milliseconds to reduce camera fps (default: not throttling)
        throttle?: number,
        // use http polling instead of streaming for input JPEGs
        poll?: boolean,
        // sets up the camera as a multiplex and rotate through the given cameras.
        multiplex?: Array<{
          // camera group
          group: string,
          // camera name
          camera: string,
          // camera display duration in milliseconds (default: 1000)
          duration?: number
        }>,
        ptz?: { // optional camera position control
          type: 'Axis' // only Axis cameras supported for the moment
          url: string
        },
        screenshot?: string, // camera's screenshot uri
        extra: { [key: string]: any } // extra config information, will be published in configuration object
      }>
    }
  },
  credentials: {
    [ camName: string ] : { // name of the relevant camera
      user: string,
      password: string
    }
  }
}
```
(*) Some camera stream may not fulfill the specifications properly. For details see https://datatracker.ietf.org/doc/html/rfc2046

Example:

```js
const
  app = require('express')(),
  { CamService } = require('cam-express');
  expressWS = require('express-ws'),

// Cameras configuration
const config = {
  groups: {
    'group1': {
      cameras: [
        { name: 'cam1', url: 'http://<host>:<port>/path/to/mjpeg/cam1' },
        { name: 'cam2', url: 'http://<host>:<port>/path/to/mjpeg/cam2' }
      ]
    },
    'group2': {
      cameras: [
        {
          name: 'cam3',
          url: 'http://<host>:<port>/path/to/jpeg/cam3',
          throttle: 2000,
          poll: true
        },
        {
          name: 'channel1',
          multiplex: [
            { group: 'group1', camera: 'cam1', duration: 4000 },
            { group: 'group2', camera: 'cam3' },
          ]
        }
      ]
    }
  },
  credentials: {
    'cam1': { user: 'USER1', password: 'PASS1' },
    'cam2': { user: 'USER2', password: 'PASS2' },
    'cam3': { user: 'USER3', password: 'PASS3' }
  }
}

// Swagger infos - optionnal (see https://swagger.io/specification/#info-object-example)
const options = {
  definition: {
    info: {
      title: 'cam-express-demo',
      version: '0.1.0'
    }
  }
}

// Swagger customisation - optionnal (see https://swagger.io/specification/#paths-object-example)
const extra = {
  paths: {
    "/config": {
      "get": {
          "description": "Custom description for the /config path"
      }
    }
  }
}

const camService = new CamService(config, options, extra);

expressWS(app);
camService.register(app);

app.listen(3000);

/* Camera streams can be retrieved at:
 - http://localhost:3000/cameras/group1/cam1 (HTTP)
 - ws://localhost:3000/cameras/group1/cam1 (WebSocket)
 - http://localhost:3000/cameras/group1/cam2 (HTTP)
 - ws://localhost:3000/cameras/group1/cam2 (WebSocket)
 - http://localhost:3000/cameras/group2/cam3 (HTTP)
 - ws://localhost:3000/cameras/group2/cam3 (WebSocket)
*/

// release once done: camService.release();
```

### MJPEG Consumer

To consume a MJPEG stream and emit frames.

The following options may be passed to the `MJPEG Stream consumer` / `HTTP Consumer`:

```ts
/*
* MJPEG Streaming => `camera.poll: false|undefined`
**/
interface MJPEGConsumerOptions {
  // camera stream source
  src: string,
  // authorization credentials for accessing camera stream
  auth?: { user: string, password: string },
  // enable the use of 'Content-Length' header field when extracting frames
  // from MJPEG stream (default: enabled)
  contentLength?: boolean,
  // boundary delimiter used in MJPEG stream fragmentation (default: retrieved from the header)
  boundary?: string // (*)
  // string representing the separator between the header and the body of a frame (default: '\r\n\r\n')
  headerDelimiter?: string,
  // force frame detection using the JPEG magic numbers (default: false)
  jpegMagicBytes?: boolean,
  // frames throttle in milliseconds to reduce camera fps (default: not throttling)
  throttle?: number
}

/*
* HTTP Polling => `camera.poll: true`
**/
interface HTTPConsumerOptions {
  // camera stream source
  src: string,
  // authorization credentials for accessing camera stream
  auth?: { user: string, password: string },
  // frames throttle in milliseconds to reduce camera fps (default: not throttling)
  throttle?: number
}
```

Example:
```js
const { MJPEGConsumer } = require('cam-express');

const mjpegConsumer = new MJPEGConsumer({
  src: 'http(s)://<host>:<port>/path/to/mjpeg/camera',
  auth: { user: 'user', password: 'pass' }
});

let lastFrame; // Buffer

mjpegConsumer
.on('frame', (frame) => {
  lastFrame = frame;
  console.log('MJPEG frame received');
})
.on('error', (err) => console.warn(`MJPEG stream error: ${err.message}`))
.on('ended', () => console.log('MJPEG stream stopped')))
.once('close', () => mjpegConsumer.removeAllListeners())
.connect();

// disconnect if no longer needed
mjpegConsumer.disconnect();
```

### PTZ Camera Control

Once enabled PTZ cameras can be controlled using `http(s)://<server>:<port>/cameras/<group>/<camera>/ptz` endpoint.

Commands are documented in `api-docs` (see Documentation below).

Here are some example PTZ commands:
```bash
# get limits
curl -X GET -vv 'http://localhost:8080/cameras/group1/cam1/ptz/limits'

# get current status
curl -X GET -vv 'http://localhost:8080/cameras/group1/cam1/ptz'

# Absolute zoom/pan/tilt
curl -X PUT -vv 'http://localhost:8080/cameras/group1/cam1/ptz' \
  -H 'Content-Type: application/json' \
  -d '{ "tilt": 42, "pan": 42, "zoom": 42 }'

# Relative zoom/pan/tilt
curl -X POST -vv 'http://localhost:8080/cameras/group1/cam1/ptz' \
  -H 'Content-Type: application/json' \
  -d '{ "tilt": 10, "pan": -10, "zoom": 20 }'
```

### Screenshot API

Screenshot API can be enabled by adding `screenshot` option in camera description, ex:
```js
const config = {
  groups: {
    'group1': {
      cameras: [
        { name: 'cam1', url: 'http://<host>:<port>/path/to/mjpeg/cam1',
          screenshot: 'http://<host>:<port>/path/to/image/cam1/screenshot.jpeg'
        }
      ]
    }
  }
}
```

**Note:** Proper credentials will be applied when accessing the resource.

To retrieve a screenshot:
```bash
curl -vv 'http://localhost:8080/cameras/group1/cam1/screenshot' -o test.jpeg
```

### MJPEG Camera client

The MJPEG camera can be displayed using the \<img\> HTML element as shown in the example below:
```html
<!DOCTYPE html>
<html>
  <body>
    <div>
      <h4>MJPEG through HTTP</h4>
      <img src="http(s)://<server>:<port>/cameras/<group>/<camera>">
    </div>
    <div>
      <h4>MJPEG through WebSocket</h4>
      <img id="mjpegWS" src="">
    </div>

    <script>
      const img = document.getElementById("mjpegWS");
      const ws = new WebSocket("ws(s)://<server>:<port>/cameras/<group>/<camera>");
      ws.onmessage = function (event) {
        if (event.data) {
          const tmp = img.src;
          img.src = URL.createObjectURL(event.data);
          URL.revokeObjectURL(tmp);
        }
      };
      ws.onclose = function (err) {
        URL.revokeObjectURL(img.src);
        img.src = "";
      };
    </script>
  </body>
</html>
```

### Documentation

An open-api json file is generated at runtime with the static (`config`) and dynamic (`groups/cams in config`) paths.

The swagger website can be accessed at the `/api-docs`, and the json schema at `/api-docs.json`.

### Testing

To run unit-tests (in development mode):
```bash
# Run unit-tests tests
npm test
```
